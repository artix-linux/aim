/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package bashlink_test

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"gitlab.org/artix-linux/aim/internal/platform/bashlink"
)

const (
	succeed = "\x1b[32m\u2713\x1b[0m"
	failed  = "\x1b[31m\u2717\x1b[0m"
)

// TestBashLinks tests the Go <-> bash context links
func TestBashLinks(t *testing.T) {

	tests := []struct {
		success         bool
		expected        int
		name            string
		script          string
		sources         string
		stdin           string
		executablePath  string
		expected_output string
		expected_err    error
		args            []string
		exportVars      map[string]string
		exportFunc      map[string]func([]string)
	}{
		{
			name:            "HelloStdout",
			script:          "hello.sh",
			sources:         `main() { echo "hello"; }`,
			success:         true,
			expected:        0,
			expected_err:    nil,
			expected_output: "hello\n",
		},
		{
			name:            "HelloStdin",
			script:          "cat.sh",
			sources:         "main() { cat; }",
			stdin:           "hello\n",
			success:         true,
			expected:        0,
			expected_err:    nil,
			expected_output: "hello\n",
		},
		{
			name:            "TestEnvironment",
			script:          "foobar.sh",
			sources:         "main() { echo $FOOBAR; }",
			success:         true,
			expected:        0,
			expected_err:    nil,
			expected_output: "Artix's Installer says, \"$X=1\"\n",
			exportVars:      map[string]string{"FOOBAR": "Artix's Installer says, \"$X=1\""},
		},
		{
			name:            "TestOddArgs",
			script:          "printf.sh",
			sources:         `main() { printf "arg: <%s>" "$@"; }`,
			success:         true,
			expected:        0,
			expected_err:    nil,
			expected_output: "arg: <hel\n\\'lo>",
			args:            []string{"hel\n\\'lo"},
		},
		{
			name:            "TestFuncCallback",
			script:          "",
			sources:         "",
			success:         true,
			expected:        0,
			expected_err:    nil,
			expected_output: "::: myfunc abc 123\n",
			args:            []string{"abc", "123"},
			executablePath:  "/bin/echo",
			exportFunc: map[string]func(args []string){
				"myfunc": func(args []string) {
					return
				},
			},
		},
	}

	t.Log("Given the need to execute Go defined functions into Bash scripts")
	{
		for i, tt := range tests {
			tf := func(t *testing.T) {
				t.Logf("\tTest: %d\tWhen executing script %s (%s)", i, tt.script, tt.sources)
				{
					var testScriptFile string
					if tt.script != "" {
						testScriptFile = filepath.Join("/tmp", tt.script)
						if err := ioutil.WriteFile(testScriptFile, []byte(tt.sources), 0644); err != nil {
							t.Fatalf("\t%s\tCan't create test script file %s: %v", failed, testScriptFile, err)
						}

						defer func() {
							os.RemoveAll(testScriptFile)
						}()
					}

					bash, err := bashlink.New("/bin/bash")
					if err != nil {
						t.Fatalf("\t%s\tShould be able to create a new Bash context: %v", failed, err)
					}

					if testScriptFile != "" {
						if err := bash.Source(testScriptFile); err != nil {
							t.Fatalf("\t%s\tShould be able to source the Bash script %s: %v", failed, testScriptFile, err)
						}
					}

					if tt.executablePath != "" {
						bash.ExecutablePath = tt.executablePath
					}

					if tt.stdin != "" {
						bash.Stdin = bytes.NewBufferString(tt.stdin)
					}

					if tt.exportVars != nil {
						for key, value := range tt.exportVars {
							bash.Export(key, value)
						}
					}

					var stdout bytes.Buffer
					bash.Stdout = &stdout

					var runErr error
					var status int
					if tt.exportFunc != nil {
						var fname string
						var fargs func([]string)

						for key, value := range tt.exportFunc {
							fname = key
							fargs = value
							break
						}

						bash.ExportFunc(fname, fargs)

						status, runErr = bash.Run(fname, tt.args)
					} else {
						status, runErr = bash.Run("main", tt.args)
					}

					if runErr != nil {
						t.Fatalf("\t%s\tCould not run main: %v", failed, err)
					}
					if status != tt.expected {
						t.Fatalf("\t%s\tExpected result status code %d got %d", failed, tt.expected, status)
					}

					if stdout.String() != tt.expected_output {
						t.Fatalf("\t%s\tExpected output %q but got %q", failed, tt.expected_output, stdout.String())
					}

					t.Logf("\t%s\tExpected result and gotten result match", succeed)
				}
			}

			t.Run(tt.name, tf)
		}

		// Special case for TestFuncHandling
		t.Run("TestFuncHandling", func(t *testing.T) {
			t.Logf("Test: %d\tWhen handling function handling", len(tests)+1)
			{
				exit := make(chan int, 1)
				bash, err := bashlink.New("/bin/bash")
				if err != nil {
					t.Fatalf("\t%s\tShould be able to create a new Bash context: %v", failed, err)
				}

				bash.ExportFunc("test-success", func(args []string) {
					exit <- 0
				})

				bash.ExportFunc("test-fail", func(args []string) {
					exit <- 2
				})

				bash.HandleFuncs([]string{"thisprogram", ":::", "test-success"})
				status := <-exit
				if status != 0 {
					t.Fatalf("\t%s\tExpected exit status to be 0 got %d", failed, status)
				}

				bash.HandleFuncs([]string{"thisprogram", ":::", "test-fail"})
				status = <-exit
				if status != 2 {
					t.Fatalf("\t%s\tExpected exit status to be 2 got %d", failed, status)
				}

				t.Logf("\t%s\tExpected exit status matches gotten statuses", succeed)
			}
		})
	}
}
