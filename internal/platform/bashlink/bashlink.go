/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package bashlink

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"sync"
	"syscall"

	"github.com/pkg/errors"
)

// NOTE: Based on previous work by Jeff Lindsay
// 		 Check https://github.com/progrium/go-basher for more info

// Exported function type alias
type exportedFunction = func([]string)

// this map contains the common commands used in environment files
var envCommands = [][]byte{
	[]byte("unset BASH_ENV\n"),
	[]byte(fmt.Sprintf("export SELF=%s\n", os.Args[0])),
	[]byte("export SELF_EXECUTABLE='%s'\n"),
}

// Context is a value holding a Bash interpreter and environmen
type Context struct {
	BashExecutablePath string
	ExecutablePath     string

	Stdin  io.Reader
	Stdout io.Writer
	Stderr io.Writer

	vars    sync.Map
	scripts sync.Map
	funcs   sync.Map
}

// New creates a new Context that will use the given bash executable,
func New(bashExecutablePath string) (*Context, error) {

	executable, err := os.Executable()
	if err != nil {
		return nil, errors.Wrap(err, "while getting executable path")
	}

	ctx := Context{
		BashExecutablePath: bashExecutablePath,
		ExecutablePath:     executable,
		Stdin:              os.Stdin,
		Stdout:             os.Stdout,
		Stderr:             os.Stderr,
	}

	return &ctx, nil
}

// Source adds a shell script to the Context environment
func (c *Context) Source(filepath string) error {

	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}
	c.scripts.Store(filepath, data)
	return nil
}

// Export mimics the export command adding a variable to the context
func (c *Context) Export(name, value string) {

	c.vars.Store(name, value)
}

// ExportFunc registers a function within the Context that will produce
// a Bash function in the environment that calls back into the Go execution context
func (c *Context) ExportFunc(name string, fn exportedFunction) {

	c.funcs.Store(name, fn)
}

// HandleFuncs handles any callbacks to Go functions registered with ExportFunc
func (c *Context) HandleFuncs(args []string) bool {

	for i, arg := range args {
		if arg == ":::" && len(args) > i+1 {
			var fn exportedFunction
			c.funcs.Range(func(k, v interface{}) bool {
				cmd, ok := k.(string)
				if !ok {
					return false
				}

				if cmd == args[i+1] {
					f, ok := v.(exportedFunction)
					if !ok {
						return false
					}

					fn = f
					return false
				}
				return true
			})

			if fn == nil {
				return false
			}

			// execute stored function
			fn(args[i+2:])
		}
	}

	return false
}

// Run runs a command in Bash from this Context
func (c *Context) Run(command string, args []string) (int, error) {

	envFile, err := c.buildEnvFile()
	if err != nil {
		return -1, errors.Wrapf(err, "while executing commnd %s(%v)", command, args)
	}
	defer os.Remove(envFile)

	argString := ""
	for _, arg := range args {
		argString = fmt.Sprintf("%s '%s'", argString, strings.Replace(arg, "'", "'\\''", -1))
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals)

	cmd := exec.Command(c.BashExecutablePath, "-c", fmt.Sprintf("%s%s", command, argString))
	cmd.Env = []string{fmt.Sprintf("BASH_ENV=%s", envFile)}
	cmd.Stdin = c.Stdin
	cmd.Stdout = c.Stdout
	cmd.Stderr = c.Stderr
	if err := cmd.Start(); err != nil {
		return -1, errors.Wrapf(err, "while starting command %s(%v)", command, args)
	}

	go func() {

		for sig := range signals {
			cmd.Process.Signal(sig)
			if cmd.ProcessState != nil && !cmd.ProcessState.Exited() {
				cmd.Process.Signal(sig)
			}
		}
	}()

	return exitStatus(cmd.Wait())
}

// builds the environment file used by Run
func (c *Context) buildEnvFile() (string, error) {

	file, err := ioutil.TempFile(os.TempDir(), "bashenv.")
	if err != nil {
		return "", errors.Wrap(err, "while creating bashenv. file")
	}
	defer file.Close()

	// variables
	for i := 0; i < len(envCommands); i++ {
		if i != 2 {
			file.Write(envCommands[i])
			continue
		}
		file.Write([]byte(fmt.Sprintf(string(envCommands[i]), c.ExecutablePath)))
	}

	c.vars.Range(func(k, v interface{}) bool {
		key, ok := k.(string)
		if !ok {
			return false
		}

		value, ok := v.(string)
		if !ok {
			return false
		}

		kvp := fmt.Sprintf("%s=%s", key, value)
		file.Write([]byte(fmt.Sprintf("export %s'\n", strings.Replace(
			strings.Replace(kvp, "'", "\\'", -2), "=", "=$'", 1,
		))))

		return true
	})

	// functions
	c.funcs.Range(func(k, v interface{}) bool {
		cmd, ok := k.(string)
		if !ok {
			return false
		}

		file.Write([]byte(fmt.Sprintf("%s() { $SELF_EXECUTABLE ::: %s \"$@\"; }\n", cmd, cmd)))
		return true
	})

	// scripts
	c.scripts.Range(func(k, v interface{}) bool {
		data, ok := v.([]byte)
		if !ok {
			return false
		}

		file.Write(append(data, '\n'))
		return true
	})

	return file.Name(), nil
}

// retrieve the shell function exit status
func exitStatus(err error) (int, error) {

	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				return int(status.ExitStatus()), nil
			}
		}
		return 0, err
	}

	return 0, nil
}
