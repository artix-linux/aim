/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package bash_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.org/artix-linux/aim/internal/module/bash"
)

const (
	succeed   = "\x1b[32m\u2713\x1b[0m"
	failed    = "\x1b[31m\u2717\x1b[0m"
	filesPath = "/tmp/.aim/tests"
)

var (
	test_bash_success_load            = []byte("run() {\n    exit 10\n}")
	test_bash_module_execution        = []byte("run() {\n    exit 129\n}\n")
	test_bash_module_execution_no_run = []byte("norun() {\n    exit 0\n}\n")
)

// some helper functions
func pre(path, contents []byte) error {
	if err := os.MkdirAll("/tmp/.aim/tests", 0777); err != nil {
		return err
	}

	return ioutil.WriteFile(string(path), contents, 0744)
}

func post() error {
	return os.RemoveAll("/tmp/.aim/tests")
}

// TestLoadBash tests Bash module loading
func TestLoadBash(t *testing.T) {

	successScript := "/tmp/.aim/tests/test_bash_success_load.sh"
	tests := []struct {
		name       string
		modulePath string
		success    bool
		expected   error
		preArgs    [][]byte
	}{
		{
			name:       "TestSuccessLoad",
			modulePath: successScript,
			success:    true,
			expected:   nil,
			preArgs:    [][]byte{[]byte(successScript), test_bash_success_load},
		},
		{
			name:       "FileDoesNotExists",
			modulePath: "/tmp/.aim/tests/i_dont_exists.sh",
			success:    false,
			expected:   fmt.Errorf("while opening Bash module FileDoesNotExists from /tmp/.aim/tests/i_dont_exists.sh: stat /tmp/.aim/tests/i_dont_exists.sh: no such file or directory"),
			preArgs:    nil,
		},
		{
			name:       "EmptyModuleName",
			modulePath: "",
			success:    false,
			expected:   fmt.Errorf("module name is empty"),
			preArgs:    nil,
		},
	}

	t.Log("Given the need to load Bash AIM modules from the file system")
	{
		for i, tt := range tests {
			tf := func(t *testing.T) {
				defer func() {
					// clean files even if we panic
					if r := recover(); r != nil {
						post()
						t.Fatalf("\t%s\tPanic recover: %v", failed, r)
					}
				}()

				t.Logf("\tTest: %d\tWhen loading module %s", i, tt.modulePath)
				{
					if tt.preArgs != nil {
						if err := pre(tt.preArgs[0], tt.preArgs[1]); err != nil {
							t.Fatalf("\t%s\tPrefunction hook failed: %v", failed, err)
						}
						defer post()
					}

					scriptFile := filepath.Base(tt.modulePath)
					if scriptFile == "." {
						scriptFile = ""
					}

					scriptExt := filepath.Ext(tt.modulePath)
					bashMod := bash.Bash{
						Name:        tt.name,
						Description: "A Simple test module",
						Path:        filesPath,
						ModuleName:  strings.Replace(scriptFile, scriptExt, "", 1),
					}

					err := bashMod.Load()
					if tt.success && tt.expected != err {
						t.Fatalf("\t%s\tShould be loaded successfully: %v", failed, err)
					} else if !tt.success && err == nil {
						t.Fatalf("\t%s\tShould not be able to load module", failed)
					} else if !tt.success && tt.expected.Error() != err.Error() {
						t.Fatalf("\t%s\tExpected:\n\t%s\nbut got\n\t%s", failed, tt.expected, err)
					}

					if tt.success {
						t.Logf("\t%s\tShould be loaded successfully", succeed)
					} else {
						t.Logf("\t%s\tShould not be loaded successfully", succeed)
					}
				}
			}
			t.Run(tt.name, tf)
		}
	}
}

// TestExecBash tests Bash module execution
func TestExecBash(t *testing.T) {

	successModule := "/tmp/.aim/tests/test_success_module_execution.sh"
	noRunModule := "/tmp/.aim/tests/test_module_execution_no_run.sh"
	tests := []struct {
		name         string
		modulePath   string
		success      bool
		expected     int
		expected_err error
		preArgs      [][]byte
	}{
		{
			name:         "TestSuccessExecution",
			modulePath:   successModule,
			success:      true,
			expected:     129,
			expected_err: nil,
			preArgs:      [][]byte{[]byte(successModule), test_bash_module_execution},
		},
		{
			name:         "TestNoRunExecution",
			modulePath:   noRunModule,
			success:      true,
			expected:     127,
			expected_err: nil,
			preArgs:      [][]byte{[]byte(noRunModule), test_bash_module_execution_no_run},
		},
	}

	t.Log("Given the need to execute Bash AIM modules from the file system")
	{
		for i, tt := range tests {
			tf := func(t *testing.T) {
				t.Logf("\tTest: %d\tWhen executing module %s", i, tt.name)
				{
					if tt.preArgs != nil {
						if err := pre(tt.preArgs[0], tt.preArgs[1]); err != nil {
							t.Fatalf("\t%s\tPrefunction hook failed: %v", failed, err)
						}
						defer post()
					}

					scriptFile := filepath.Base(tt.modulePath)
					scriptExt := filepath.Ext(tt.modulePath)
					bashMod := bash.Bash{
						Name:        tt.name,
						Description: "A Simple test module",
						Path:        filesPath,
						ModuleName:  strings.Replace(scriptFile, scriptExt, "", 1),
					}

					if err := bashMod.Load(); err != nil {
						t.Fatalf("\t%s\tShould be able to load module %s: %v", failed, tt.name, err)
					}
					t.Logf("\t%s\tShould be able to load module %s", succeed, tt.name)

					ret, err := bashMod.Exec()
					if tt.success && err != nil {
						t.Errorf("\t%s\tShould be able to execute module %s: %v", failed, tt.name, err)
					} else if !tt.success && err == nil {
						t.Errorf("\t%s\tShould not be able to execute module %s", failed, tt.name)
					} else if !tt.success && tt.expected_err.Error() != err.Error() {
						t.Errorf("\t%s\tExpected:\n\t%s\nbut got:\n\t%s", failed, tt.expected_err, err)
					}

					if tt.expected != ret {
						t.Errorf("\t%s\tExpected return value to be %d but got %d", failed, tt.expected, ret)
					}

					t.Logf("\t%s\tExpected return value and return value match", succeed)
				}
			}

			t.Run(tt.name, tf)
		}
	}
}
