/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package bash

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"gitlab.org/artix-linux/aim/internal/platform/bashlink"
)

// Bash structure defines a Bash executable module for AIM
type Bash struct {
	Name        string
	Description string
	Path        string
	ModuleName  string
	ctx         *bashlink.Context
}

// Load loads a Bash module from the filesystem
func (b *Bash) Load() error {

	if b.ModuleName == "" {
		return fmt.Errorf("module name is empty")
	}

	moduleFilePath := fmt.Sprintf("%s.sh", filepath.Join(b.Path, b.ModuleName))
	_, err := os.Stat(moduleFilePath)
	if err != nil && os.IsNotExist(err) {
		return errors.Wrapf(err, "while opening Bash module %s from %s", b.Name, moduleFilePath)
	}

	ctx, err := bashlink.New("/bin/bash")
	if err != nil {
		return errors.Wrap(err, "while creating Bash Context")
	}
	b.ctx = ctx

	if err := b.ctx.Source(moduleFilePath); err != nil {
		return errors.Wrap(err, "while sourcing module Bash script")
	}

	return nil
}

// Exec executes the module main entry point and reutnrs it's exit code
func (b *Bash) Exec() (int, error) {

	status, err := b.ctx.Run("run", []string{})
	if err != nil {
		return -1, errors.Wrapf(err, "while running Bash script %s", b.Name)
	}

	return status, nil
}
