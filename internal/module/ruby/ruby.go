/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package ruby

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	mruby "github.com/mitchellh/go-mruby"
	"github.com/pkg/errors"
)

// Ruby structure defines a Ruby executable module for AIM
type Ruby struct {
	Name        string
	Description string
	Path        string
	ModuleName  string
	mrb         *mruby.Mrb
	code        *mruby.MrbValue
}

// Load loads a Ruby module from the filesystem
func (r *Ruby) Load() error {

	if r.ModuleName == "" {
		return fmt.Errorf("module name is empty")
	}

	moduleFilePath := fmt.Sprintf("%s.rb", filepath.Join(r.Path, r.ModuleName))
	_, err := os.Stat(moduleFilePath)
	if err != nil && os.IsNotExist(err) {
		return errors.Wrapf(err, "while opening Ruby module %s from %s", r.Name, moduleFilePath)
	}

	r.mrb = mruby.NewMrb()
	ctx := mruby.NewCompileContext(r.mrb)
	defer ctx.Close()

	ctx.SetFilename(filepath.Base(moduleFilePath))
	parser := mruby.NewParser(r.mrb)
	defer parser.Close()

	data, readErr := ioutil.ReadFile(moduleFilePath)
	if readErr != nil {
		return errors.Wrap(err, "while reading module contents")
	}

	if _, err := parser.Parse(string(data), ctx); err != nil {
		return errors.Wrapf(err, "while parsing %s", r.Name)
	}

	// generate mruby proc and store it
	r.code = parser.GenerateCode()

	return nil
}

// Exec executes the module main entry point and retunrs it's exit code
func (r *Ruby) Exec() (int, error) {

	var status int
	_, err := r.mrb.Run(r.code, nil)
	if err != nil {
		return -1, errors.Wrap(err, "while executing Ruby script")
	}

	result, err := r.mrb.LoadString("run")
	if err != nil {
		return -1, errors.Wrap(err, "while running main entry point")
	}

	if decErr := mruby.Decode(&status, result); decErr != nil {
		return -1, errors.Wrapf(decErr, "while decoding MRuby value %+v", result)
	}

	return status, nil
}
