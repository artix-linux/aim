/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package ruby_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.org/artix-linux/aim/internal/module/ruby"
)

const (
	succeed   = "\x1b[32m\u2713\x1b[0m"
	failed    = "\x1b[31m\u2717\x1b[0m"
	filesPath = "/tmp/.aim/tests"
)

var (
	test_ruby_success_load               = []byte("def run\n    exit 10\nend\n")
	test_ruby_invalid_code               = []byte("def a")
	test_ruby_module_execution           = []byte("def run\n    return 129\nend\n")
	test_ruby_module_execution_no_run    = []byte("def norun\n    return 0\nend\n")
	test_ruby_module_execution_exception = []byte("def run\n    raise 'nope'\nend\n")
)

// some helper functions
func pre(path, contents []byte) error {
	if err := os.MkdirAll("/tmp/.aim/tests", 0777); err != nil {
		return err
	}

	return ioutil.WriteFile(string(path), contents, 0744)
}

func post() error {
	return os.RemoveAll("/tmp/.aim/tests")
}

// TestLoadRuby tests Ruby module loading
func TestLoadRuby(t *testing.T) {

	successScript := "/tmp/.aim/tests/test_ruby_success_load.rb"
	failedScript := "/tmp/.aim/tests/test_invalid_ruby_code.rb"
	tests := []struct {
		name       string
		modulePath string
		success    bool
		expected   error
		preArgs    [][]byte
	}{
		{
			name:       "TestSuccessLoad",
			modulePath: successScript,
			success:    true,
			expected:   nil,
			preArgs:    [][]byte{[]byte(successScript), test_ruby_success_load},
		},
		{
			name:       "FileDoesNotExists",
			modulePath: "/tmp/.aim/tests/i_dont_exists.rb",
			success:    false,
			expected:   fmt.Errorf("while opening Ruby module FileDoesNotExists from /tmp/.aim/tests/i_dont_exists.rb: stat /tmp/.aim/tests/i_dont_exists.rb: no such file or directory"),
			preArgs:    nil,
		},
		{
			name:       "EmptyModuleName",
			modulePath: "",
			success:    false,
			expected:   fmt.Errorf("module name is empty"),
			preArgs:    nil,
		},
		{
			name:       "InvalidRubyCode",
			modulePath: failedScript,
			success:    false,
			expected:   fmt.Errorf("while parsing InvalidRubyCode: Ruby parse error!\n\nline 0:0: \n"),
			preArgs:    [][]byte{[]byte(failedScript), test_ruby_invalid_code},
		},
	}

	t.Log("Given the need to load Bash AIM modules from the file system")
	{
		for i, tt := range tests {
			tf := func(t *testing.T) {
				defer func() {
					// clean files even if we panic
					if r := recover(); r != nil {
						post()
						t.Fatalf("\t%s\tPanic recover: %v", failed, r)
					}
				}()

				t.Logf("\tTest: %d\tWhen loading module %s", i, tt.modulePath)
				{
					if tt.preArgs != nil {
						if err := pre(tt.preArgs[0], tt.preArgs[1]); err != nil {
							t.Fatalf("\t%s\tPrefunction hook failed: %v", failed, err)
						}
						defer post()
					}

					scriptFile := filepath.Base(tt.modulePath)
					if scriptFile == "." {
						scriptFile = ""
					}

					scriptExt := filepath.Ext(tt.modulePath)
					rubyMod := ruby.Ruby{
						Name:        tt.name,
						Description: "A Simple test module",
						Path:        filesPath,
						ModuleName:  strings.Replace(scriptFile, scriptExt, "", 1),
					}

					err := rubyMod.Load()
					if tt.success && tt.expected != err {
						t.Fatalf("\t%s\tShould be loaded successfully: %v", failed, err)
					} else if !tt.success && err == nil {
						t.Fatalf("\t%s\tShould not be able to load module", failed)
					} else if !tt.success && tt.expected.Error() != err.Error() {
						t.Fatalf("\t%s\tExpected:\n\t%q\nbut got\n\t%q", failed, tt.expected, err)
					}

					if tt.success {
						t.Logf("\t%s\tShould be loaded successfully", succeed)
					} else {
						t.Logf("\t%s\tShould not be loaded successfully", succeed)
					}
				}
			}
			t.Run(tt.name, tf)
		}
	}
}

// TestExecRuby tests Python module execution
func TestExecRuby(t *testing.T) {

	successModule := "/tmp/.aim/tests/test_success_module_execution.rb"
	noRunModule := "/tmp/.aim/tests/test_module_execution_no_run.rb"
	exceptionModule := "/tmp/.aim/tests/test_module_execution_exception.rb"
	tests := []struct {
		name         string
		modulePath   string
		success      bool
		expected     int
		expected_err error
		preArgs      [][]byte
	}{
		{
			name:         "TestSuccessExecution",
			modulePath:   successModule,
			success:      true,
			expected:     129,
			expected_err: nil,
			preArgs:      [][]byte{[]byte(successModule), test_ruby_module_execution},
		},
		{
			name:         "TestNoRunExecution",
			modulePath:   noRunModule,
			success:      false,
			expected:     -1,
			expected_err: fmt.Errorf("while running main entry point: undefined method 'run' for main"),
			preArgs:      [][]byte{[]byte(noRunModule), test_ruby_module_execution_no_run},
		},
		{
			name:         "TestExecutionException",
			modulePath:   exceptionModule,
			success:      false,
			expected:     -1,
			expected_err: fmt.Errorf("while running main entry point: nope"),
			preArgs:      [][]byte{[]byte(exceptionModule), test_ruby_module_execution_exception},
		},
	}

	t.Log("Given the need to execute Python AIM modules from the file system")
	{
		for i, tt := range tests {
			tf := func(t *testing.T) {
				t.Logf("\tTest: %d\tWhen executing module %s", i, tt.name)
				{
					if tt.preArgs != nil {
						if err := pre(tt.preArgs[0], tt.preArgs[1]); err != nil {
							t.Fatalf("\t%s\tPrefunction hook failed: %v", failed, err)
						}
						defer post()
					}

					scriptFile := filepath.Base(tt.modulePath)
					scriptExt := filepath.Ext(tt.modulePath)
					rubyMod := ruby.Ruby{
						Name:        tt.name,
						Description: "A Simple test module",
						Path:        filesPath,
						ModuleName:  strings.Replace(scriptFile, scriptExt, "", 1),
					}

					if err := rubyMod.Load(); err != nil {
						t.Fatalf("\t%s\tShould be able to load module %s: %v", failed, tt.name, err)
					}
					t.Logf("\t%s\tShould be able to load module %s", succeed, tt.name)

					ret, err := rubyMod.Exec()
					if tt.success && err != nil {
						t.Fatalf("\t%s\tShould be able to execute module %s: %v", failed, tt.name, err)
					} else if !tt.success && err == nil {
						t.Fatalf("\t%s\tShould not be able to execute module %s", failed, tt.name)
					} else if !tt.success && tt.expected_err.Error() != err.Error() {
						t.Fatalf("\t%s\tExpected:\n\t%s\nbut got:\n\t%s", failed, tt.expected_err, err)
					}

					if tt.expected != ret {
						t.Errorf("\t%s\tExpected return value to be %d but got %d", failed, tt.expected, ret)
					}

					t.Logf("\t%s\tExpected return value and return value match", succeed)
				}
			}

			t.Run(tt.name, tf)
		}
	}
}
