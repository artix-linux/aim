/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package python

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/sbinet/go-python"
)

// we use this variable to know if Python support is enabled and initialized
var supported = true

func init() {
	// initialize the Python embedded interpreter
	err := python.Initialize()
	if err != nil {
		supported = false
	}
}

// Enabled returns back true if Python is supported, false otherwise
func Enabled() bool {
	return supported
}

// Python structure defines a Python executable module for AIM
type Python struct {
	Name        string
	Description string
	Path        string
	ModuleName  string
	pModule     *python.PyObject
}

// Load loads a Python module from the filesystem
func (p *Python) Load() (err error) {

	if p.ModuleName == "" {
		return fmt.Errorf("module name is empty")
	}

	moduleFilePath := fmt.Sprintf("%s.py", filepath.Join(p.Path, p.ModuleName))
	_, err = os.Stat(moduleFilePath)
	if err != nil && os.IsNotExist(err) {
		return errors.Wrapf(err, "while opening Python module %s from %s", p.Name, moduleFilePath)
	}
	python.PyErr_Clear()

	if err := p.sysPathCheck(); err != nil {
		return err
	}

	pName := python.PyString_FromString(p.ModuleName)
	if pName == nil {
		return expandError(fmt.Errorf("could not create PyObject from Python module %s path %s", p.Name, p.Path))
	}

	p.pModule = python.PyImport_Import(pName)
	if p.pModule == nil {
		return expandError(fmt.Errorf("could not import Python module %s from path %s", p.Name, p.Path))
	}

	// clear the pointer and decrement the reference counter
	pName.Clear()

	return nil
}

// Exec executes the module main entry point and returns it's exit code
func (p *Python) Exec() (int, error) {
	pFunc := p.pModule.GetAttrString("run")
	if pFunc == nil || !pFunc.Check_Callable() {
		return -1, fmt.Errorf("could not find 'run' function in module %s", p.Name)
	}

	// create empty args tuple
	pArgs := python.PyTuple_New(0)

	// execute function and store return value
	pValue := pFunc.CallObject(pArgs)
	pArgs.Clear()

	ret := 0
	if pValue != nil {
		ret = python.PyInt_AsLong(pValue)
		pValue.Clear()
	}
	err := expandError(nil)

	pFunc.Clear()
	return ret, err
}

// check if a module path is on system paths and add it if not
func (p *Python) sysPathCheck() error {
	// get the path list fro mthe system module
	pPath := python.PySys_GetObject("path")

	// check for module path existence on path list
	lSize := python.PyList_Size(pPath)
	for i := 0; i < lSize; i++ {
		// item here is a borrowed reference
		item := python.PyList_GetItem(pPath, i)
		if python.PyString_AsString(item) == p.Path {
			return nil
		}
	}

	// if we find ourselves at this point is because the path is not added yet
	err := python.PyList_Append(pPath, python.PyString_FromString(p.Path))

	return err
}

// call clear oo Py_DECREF in the given values
func clean(variables ...*python.PyObject) {

	for i := range variables {
		variables[i].Clear()
	}
}

// extract exception messages from Python exceptions
func expandError(err error) error {

	exception, value, traceback := python.PyErr_Fetch()
	python.PyErr_NormalizeException(exception, value, traceback)

	if value != nil {
		exceptionMessage := python.PyString_AsString(value.Str())
		if exceptionMessage != "" && exceptionMessage != "<NULL>" {
			if err != nil {
				err = errors.Wrapf(fmt.Errorf("%s", exceptionMessage), err.Error())
			} else {
				err = fmt.Errorf("%s", exceptionMessage)
			}
		}
	}

	python.PyErr_Clear()
	clean(exception, value, traceback)
	return err
}
