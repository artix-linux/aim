/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package config

import (
	"io/ioutil"
	"os"

	"github.com/pkg/errors"
	yaml "gopkg.in/yaml.v2"
)

// default values for configuration fields
const (
	socketPath = "/run/aim/aim.socket"
	logPath    = "/var/log/aim"
	logLevel   = "normal"
	modulePath = "/usr/share/aim/modules"
)

// Settings is a simple storage of configuration options
type Settings struct {
	SocketPath  string `yaml:"socket"`
	LogPath     string `yaml:"log"`
	LogLevel    string `yaml:"log_level"` // quiet, normal, verbose, debug
	ModulesPath string `yaml:"modules_path"`
}

// Load loads the  YAML text from the given file, unmarshal and returns it
func Load(filename string) (Settings, error) {
	var settings Settings

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return settings, errors.Wrap(err, "can not open configuration file")
		}
		return settings, errors.Wrap(err, "unknown error while trying to open configuration file")
	}

	if err := yaml.Unmarshal(data, &settings); err != nil {
		return settings, errors.Wrapf(err, "while trying to unmarshal YAML string:\n%s\n", data)
	}

	// apply defaults
	if settings.SocketPath == "" {
		settings.SocketPath = socketPath
	}

	if settings.LogPath == "" {
		settings.LogPath = logPath
	}

	if settings.LogLevel == "" {
		settings.LogLevel = logLevel
	}

	if settings.ModulesPath == "" {
		settings.ModulesPath = modulePath
	}

	return settings, nil
}
