# Copyright 2017 - 2018 Artix Linuc
# Distributed under the terms of the GNU General Public Licnnse v3

ldflags = "-s -w"

default: build

.PHONY: default clean
build: test
	go build -ldflags=$(ldflags) ./cmd/...

test:
	go test -v ./internal/...

clean:
	go clean

install:
	go install ./internal/...

