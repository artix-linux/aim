module gitlab.org/artix-linux/aim

go 1.12

require (
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/colour v0.0.0-20160524082231-60882d9e2721 // indirect
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1 // indirect
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mitchellh/go-mruby v0.0.0-20181003231329-cd6a04a6ea57
	github.com/pkg/errors v0.8.1
	github.com/sbinet/go-python v0.0.0-20190327143913-ddccf6692cfa
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/stretchr/testify v1.3.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405
	gopkg.in/yaml.v2 v2.2.2
)
