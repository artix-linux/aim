/*
	This file is part of Artix Installation Manager (AIM)
	Copyright (C) 2017  Oscar Campos <damnwidget@artixlinux.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"os"
	"time"

	"runtime"

	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

var (
	app     = kingpin.New("aim", "Artix Installation Manager Daemon.")
	debug   = app.Flag("debug", "Enable debug mode").Short('d').Bool()
	cfg     = app.Flag("cfg", "Configuration file").Short('c').PlaceHolder("AIM_CONFIG_FILE").Default("/etc/aim.d/aim.conf").OverrideDefaultFromEnvar("AIM_CONFIG_FILE").String()
	version = app.Flag("version", "Show version information and exit").Short('v').Bool()
)

var (
	versionNumber          = ""
	versionCommit          = ""
	versionBuildTime       = ""
	versionBuildRuntimeVer = runtime.Version()
)

// This is the main entry point for AIM daemon
func main() {
	kingpin.MustParse(app.Parse(os.Args[1:]))
	if *version {
		os.Exit(showVersion())
	}
}

// shows version information and exits
func showVersion() int {
	// make buildtime more user friendly
	t, err := time.Parse("2006-01-02T15:04:05Z0700", versionBuildTime)
	if err == nil {
		versionBuildTime = t.Format(time.ANSIC)
	}

	fmt.Printf(
		"Artix Installation Manager Daemon v%s:\n  Built on: %s\n  Built with Go: %s\n  Based on commit: %s\n",
		versionNumber, versionBuildTime, versionBuildRuntimeVer, versionCommit,
	)

	return 0
}
